### P3B115717_data.clean.R

rm(list=ls())
library(openxlsx)
library(car)

#read-in data file
dir      <- "U:/My Documents/tdx/Project7 32-MSLN-15051/"
file     <- "Client 32 MSLN-15051 preliminary data 01MAR2016.xlsx"
prj      <- "15051"
in.file  <- paste0(dir, file)
password <- "32-MSLN-15051"

library(RDCOMClient)
eApp <- COMCreate("Excel.Application")
wk <- eApp$Workbooks()$Open(Filename = in.file, Password = password)
tf<- tempfile()
wk$SaveAs(tf,3)
wk$Close(SaveChanges = F)
input.dat<- read.delim(sprintf("%s.txt", tf), header = TRUE, sep = "\t")
#input.dat <- read.table(sprintf("%s.txt", tf), header = FALSE, sep = "\t", quote = "", fill = TRUE, stringsAsFactors = FALSE)
time <- gsub("[: ]", "-", Sys.time())  #attach time to related output files.
filename <- paste0(dir, prj, "-TRACKER-", time, ".RData")
#save(input.dat,file = filename)

file2 <- "MSLN-15051 Preliminary Results 01MAR2016.xls"
in.file2 <- paste0(dir, file2)
eApp <- COMCreate("Excel.Application")
wk <- eApp$Workbooks()$Open(Filename = in.file2, Password = password)
tf<- tempfile()
wk$SaveAs(tf,3)
wk$Close(SaveChanges = F)
input.dat2<- read.delim(sprintf("%s.txt", tf), skip = 1, header = TRUE, sep = "\t")
#input.dat <- read.table(sprintf("%s.txt", tf), header = FALSE, sep = "\t", quote = "", fill = TRUE, stringsAsFactors = FALSE)
time <- gsub("[: ]", "-", Sys.time())  #attach time to related output files.
filename <- paste0(dir, prj, "-TRACKER-", time, ".RData")
#save(input.dat,file = filename)

## remove the rows without collection_date
input.dat <- input.dat[!is.na(input.dat$Study),1:27]
nr <- nrow(input.dat)  ## number of rows
nr2 <- nrow(input.dat2)

### STUDYID
df <- data.frame(STUDYID = as.character(rep(prj, nr)))
df2 <- data.frame(STUDYID = as.character(rep(prj, nr2)))

### DOMAIN
df$DOMAIN <- "XB"
df2$DOMAIN <- "XB"

### SUBJIDN
df$SUBJIDN <- as.numeric(input.dat$Bayer.provided.Subject.Identifier)
df2$SUBJIDN <- as.numeric(input.dat2$Subject.ID)

### XBSPID, XBREFID
xbspid <- input.dat$Ventana.Case.ID
df$XBREFID <- ifelse(grepl("repeat", xbspid, ignore.case = TRUE),
                     yes = "REPEAT",
                     no  = "INITIAL")
df$XBSPID <- gsub("\\(?repeat.*", "", xbspid, ignore.case = TRUE)

xbspid2 <- input.dat2$Case.ID
df2$XBREFID <- ifelse(grepl("repeat", xbspid2, ignore.case = TRUE),
                      yes = "REPEAT",
                      no  = "INITIAL")
df2$XBSPID <- gsub("\\(?repeat.*", "", xbspid2, ignore.case = TRUE)

### XBNAM, VISITNUM
df$XBNAM <- "Ventana CDx Pharma Services"
df$VISITNUM <- 0
df2$XBNAM <- "Ventana CDx Pharma Services"
df2$VISITNUM <- 0

### XB_D, XB_M, XB_Y
xb_dmy <- format(lubridate::dmy(input.dat$Collection.Date), format = "%d%m%Y")
df$XB_D <- substring(xb_dmy, 1, 2)
df$XB_M <- substring(xb_dmy, 3, 4)
df$XB_Y <- substring(xb_dmy, 5)
df$COVAL <- toupper(input.dat$Comments)  ## obs 36, 37 have more than 200 characters
df$XBCAT <- 2

xb2_dmy <- format(lubridate::dmy(input.dat2$Collection.Date), format = "%d%m%Y")
df2$XB_D <- substring(xb2_dmy, 1, 2)
df2$XB_M <- substring(xb2_dmy, 3, 4)
df2$XB_Y <- substring(xb2_dmy, 5)
df2$COVAL <- toupper(input.dat2$Comments)  ## obs 36, 37 have more than 200 characters
df2$XBCAT <- 2

### XBTESTCD, XBTEST, XBTSTDTL, XBSTDTLN
df$N   <- input.dat$H.E.Accept..Reject
df$N6  <- input.dat$Total.H.Score
df$N8  <- input.dat$Membrane.3.
df$N9  <- input.dat$Membrane.2.
df$N10 <- input.dat$Membrane.1.
df$N11 <- input.dat$Membrane.0.
df$N27 <- input.dat$Neg.Ctl.Accept..Reject
df$N29 <- gsub("^\\s+|\\s+$", "", gsub("prim([a-zA-Z])+", "", input.dat$Primarily.Apical..Circumferential..Or.both, ignore.case = TRUE))
df$N30 <- input.dat$Tumor.tissue.is.expressing.mesothelin..membrane.intensity.score.of.2..or.3...on.the.0.3.scale..on.at.least.30..of.tumor.cells..
df$N31 <- as.numeric(input.dat$Membrane.2.) + as.numeric(input.dat$Membrane.3.)
df$N25 <- NA
df$N26 <- NA
out <- tidyr::gather(df, testcode, value, N:N26)

df2$N   <- input.dat2$H.E.Accept.Reject
df2$N6  <- NA
df2$N9  <- NA
df2$N8  <- NA
df2$N10 <- NA
df2$N11 <- NA
df2$N27 <- input.dat2$Neg.Ctl.Accept..Reject
df2$N29 <- NA
df2$N30 <- NA
df2$N31 <- NA
df2$N25 <- input.dat2$Cytoplasmic.Intensity...0.3.gestalt.
df2$N26 <- input.dat2$Cytoplasmic...positive
out2 <- tidyr::gather(df2, testcode, value, N:N26)

out1 <- rbind(out, out2)
out1$XBSTDTLN <- as.numeric(gsub("N", "", out1$testcode))

xbn <- c(6, 8, 9, 10, 11, 27, 29, 30, 31, 25, 26)
xbl <- c("H_Score", "% of tumor membrane expressing 3+", "% of tumor membrane expressing 2+",
         "% of tumor membrane expressing 1+", "% of tumor membrane expressing 0", "Negative Control",
         "Membrane staining pattern", "Diagnostic score", "Total percent of tumor membrane expressing
         3+ and 2+", "Predominant staining intensity", "% tumor membrane expressing predominant 
         staining intensity")

num2char <- function(num){
    return(xbl[which(xbn == num)])
}
vnum2char <- Vectorize(num2char)

out1$XBTESTCD <- ifelse(out1$XBSTDTLN %in% xbn,
                        yes = "MSLN",
                        no  = "HNE")

out1$XBTEST <- ifelse(out1$XBSTDTLN %in% xbn,
                      yes = "MESOTHELIN",
                      no  = "H&E")

out1$XBTSTDTL <- ifelse(out1$XBSTDTLN %in% xbn,
                        yes = vnum2char(out1$XBSTDTLN),
                        no  = '')

out1$XBORRESU <- ifelse(out1$XBSTDTLN %in% c(26, 11, 10, 9, 8, 31),
                        yes = "%",
                        no  = NA)

out1$XBORRES <- out1$value
out1$XBREASND <- ifelse(grepl("(NO.*TUMOR|TUMOR.*NO|FELL OFF)", out1$COVAL) & is.na(out1$XBORRESU),
                        yes = "INSUFFICIENT SAMPLE",
                        no  = ifelse(grepl("UNEVALUABLE", out1$COVAL) & is.na(out1$XBORRESU),
                                     yes = "NOT EVALUABLE",
                                     no  = ifelse(is.na(out1$XBORRESU),
                                                  yes = "NOT ANALYZED",
                                                  no  = NA)))
## out1$XBREASND <- NA
## out1$XBREASND[which(grepl("(NO.*TUMOR|TUMOR.*NO|FELL OFF)", out1$COVAL))] <-"INSUFFICIENT SAMPLE"
## out1$XBREASND[which(is.na(out1$XBREASND) & grepl("UNEVALUABLE", out1$COVAL))] <- "NOT EVALUABLE"
## out1$XBREASND[is.na(out1$XBORRESU)] <- "NOT ANALYZED"
out1$XBRESNDN <- as.character(recode(out1$XBREASND,"'INSUFFICIENT SAMPLE'=6;'NOT EVALUABLE'=9;'NOT ANALYZED'=1"))

## remove any duplicates if exists and reordering
var_list <- c("STUDYID", "DOMAIN", "SUBJIDN", "XBSPID", "XBTESTCD", "XBTEST",
              "XBTSTDTL", "XBSTDTLN", "XBORRES", "XBORRESU", "XBREASND",
              "XBRESNDN", "XBNAM", "VISITNUM", "XB_D", "XB_M", "XB_Y", "COVAL", 
              "XBCAT", "XBREFID")
numorder <- c(NA, 27, 11, 10, 9, 8, 31, 6, 29, 30, 25, 26)
finalout <- out1[order(out1$XBSPID, out1$XBREFID, match(out1$XBSTDTLN, numorder)), var_list]

################################################################################################
#Save and write.
################################################################################################

## time <- gsub("[: ]", "-", Sys.time())
## filename1 <- paste0(dir, prj, "-PRODUCTION-", time, ".RDATA")
## save(out, file = filename1)
## filename2 <- paste0(dir, prj, "-PRODUCTION-", time, ".xlsx")
## write.xlsx(out, file = filename2, showNA = FALSE, col.names = TRUE, row.names = FALSE) ## 