### From Rblogger
### http://www.r-bloggers.com/adding-motion-to-choropleths/?utm_source=feedburner&utm_medium=feed&utm_campaign=Feed%3A+RBloggers+%28R+bloggers%29

URL <- "http://graphics8.nytimes.com/newsgraphics/2016/01/15/drug-deaths/c23ba79c9c9599a103a8d60e2329be1a9b7d6994/data.json"

data("uscountygeojson")
data("unemployment")

### it is good for package jsonlite. but not working for RJSONIO
library(dplyr)
data <-  fromJSON(URL) %>% 
    tbl_df() %>% 
    gather(year, value, -fips) %>% 
    mutate(year = sub("^y", "", year),
           value = ifelse(is.na(value), 0, value))


### 
data <-  fromJSON(URL) %>%
        newJSONParser(method = "R")

a <- do.call(cbind.data.frame, data)
a <- data.frame(t(sapply(data, c)))
a <- as.data.frame(data)

a <- matrix(sapply(data, c), nrow = 14, byrow = TRUE)
b <- sapply(data, c)


library(magrittr)

############### make a random matrix  ##########
#######################################
rand_mat <- function() {
    Nrow <- sample(2:15,1)
    Ncol <- sample(2:15,1)
    rpois(Nrow*Ncol,20) %>%
        matrix(nrow = Nrow,ncol = Ncol)
}

### list to data.frame
list_to_df <- function(list_for_df) {
    list_for_df <- as.list(list_for_df)
    
    nm <- names(list_for_df)
    if (is.null(nm))
        nm <- seq_along(list_for_df)
    
    df <- data.frame(name = nm, stringsAsFactors = FALSE)
    df$value <- unname(list_for_df)
    df
}

nm <- names(data)
if (is.null(nm))
    nm <- seq_along(data)

df <- data.frame(name = nm, stringsAsFactors = FALSE)
df$value <- unname(data)

### the following method will only work if all element list have the same length 
ddf <- df[-c(73, 250, 2913, 2915), ]
a <- data.frame(t(sapply(1:nrow(ddf), FUN = function(x) unlist(ddf[x,2]))))


### the following method is not appropriate, since each element is a list
separate(data = ddf,
         col  = value,
         into = c("fips", paste0("y", 1:13 + 2001)),
         sep  = ", ",
         convert = F)

### list to dataframe using dplyr
require(dplyr)
require(tidyr)
l =list()
l[[1]] = list(member1=c(a=rnorm(1)),member2=matrix(rnorm(3),nrow=3,ncol=1,dimnames=list(c(letters[2:4]),c("sample"))))
l[[2]] = list(member1=c(a=rnorm(1)),member2=matrix(rnorm(3),nrow=3,ncol=1,dimnames=list(c(letters[2:4]),c("sample"))))
l[[3]] = list(member1=c(a=rnorm(1)),member2=matrix(rnorm(3),nrow=3,ncol=1,dimnames=list(c(letters[2:4]),c("sample"))))

a <- lapply(l, `[[`, 2) %>% 
    data.frame %>% 
    add_rownames("key") %>% 
    gather(x, value, -key) %>% 
    select(-x)

### the following method works very well except it requires the identical length of each list element
b <- sapply(1:13, FUN = function(x) cbind(lapply(data, '[[', x)))
colnames(b) <- names(data[[1]])[-14]


#### testing of tapply and aggregate
dfr <- data.frame(a=1:20, f=rep(LETTERS[1:5], each=4))
means <- tapply(dfr$a, dfr$f, mean)

aggregate(dfr$a, by = list(dfr$f), mean)
ab <- aggregate(a ~ f, data = dfr, FUN = function(x) sd(x) / mean(x))


### structure of a list
X <- list(a = ordered(c(1:30,30:1)),
          b = c("Rick","John","Allan"),
          c = diag(30),
          e = cbind(p=1008:1019,q=4)
          )

list.tree(X)

#######################################
library(jsonlite)
library(tigris)
library(ggplot2)
library(purrr)
library(tidyr)
library(dplyr)
library(sp)
library(rgeos)
library(rgdal)
library(maptools)
library(ggthemes)
library(viridis)


URL <- "http://graphics8.nytimes.com/newsgraphics/2016/01/15/drug-deaths/c23ba79c9c9599a103a8d60e2329be1a9b7d6994/data.json"
fil <- "epidemic.json"
if (!file.exists(fil)) download.file(URL, fil)

data <- fromJSON("epidemic.json")

drugs <- gather(data, year, value, -fips)
drugs$year <- sub("^y", "", drugs$year)
drugs <- filter(drugs, year != "2012")

qcty <- quietly(counties)

us <- qcty(setdiff(state.abb, c("AK", "HI")), cb=TRUE)$result
us <- suppressWarnings(SpatialPolygonsDataFrame(gBuffer(gSimplify(us, 0.1, TRUE), byid=TRUE, width=0), us@data))

us_map <- fortify(us, region="GEOID")

